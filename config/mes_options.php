<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

# forcer la compat des plugins en attendant que sedna soit compat 3.1
define('_DEV_PLUGINS', '3.1.99');

# booster les delais de syndication (en minutes)
define('_PERIODE_SYNDICATION', 10);  
define('_PERIODE_SYNDICATION_SUSPENDUE', 3*60);

# les urls chics
define('_SPIP_SCRIPT', '');

include_spip('inc/vieilles_defs');
